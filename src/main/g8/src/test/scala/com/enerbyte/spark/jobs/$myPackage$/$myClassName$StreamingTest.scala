package com.enerbyte.spark.jobs.$myPackage$

import com.enerbyte.spark.utils.SparkStreamingSpec
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.{Time, Seconds}
import org.scalatest.concurrent.Eventually
import org.scalatest.time.{Millis, Span}
import org.scalatest.{MustMatchers, GivenWhenThen, FlatSpec}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

class $myClassName$StreamingTest extends FlatSpec
with SparkStreamingSpec
with MustMatchers
with GivenWhenThen
with Eventually
{
  val windowDuration = Seconds(3)
  val sliceDuration = Seconds(1) // Will produce results every second

  implicit override val patienceConfig = PatienceConfig(timeout = scaled(Span(1500, Millis)))

  "The streaming job" should "reverse lines in a stream" in {
    Given("A stream of lines")
    val lines = mutable.Queue[RDD[String]]()

    val results = ListBuffer.empty[Array[String]]

    $myClassName$StreamingJob.run(ssc.queueStream(lines), windowDuration, sliceDuration) { (linesInWindow:RDD[String], time:Time) =>
      results += linesInWindow.map(_.reverse).collect()
    }

    ssc.start()

    When("I add lines a phrase to the stream")
    val toBeSentence = "To be or not to be."
    lines += sc.makeRDD(Seq(toBeSentence))

    Then("I get those lines reversed")
    clock.advance(sliceDuration.milliseconds)
    eventually {
      results.last must equal (Array(toBeSentence.reverse))
    }

    When("I add more lines to the stream")
    val thatsQuestionSentence = "That is the question"
    lines += sc.makeRDD(Seq(thatsQuestionSentence))

    Then("I still get those lines reversed")
    clock.advance(sliceDuration.milliseconds)
    eventually {
      results.last must equal (Array(thatsQuestionSentence.reverse))
    }
  }

}
