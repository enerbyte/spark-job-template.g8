package com.enerbyte.spark.jobs.$myPackage$

import java.sql.Time

import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkContext, SparkConf}

/**
 * Created by cebrian on 10/09/15.
 */
object $myClassName$BatchJob {

  /**
   * This is the main function for the Spark job. Remove the ssc context in case you are working
   * on any batch processing functionality
   *
   * @param args
   */
  def main(args:Array[String]) = {
    val conf = new SparkConf()
      .setAppName("$name$")
    val sc = new SparkContext(conf)

    // Create either the RDD or the DStream
    val lines = sc.textFile("s3://my-folder-with-files")

     val transformedRDD = run(lines)

    // Do something with the transformed output

    sc.stop()
  }


  // The RDD[String] is just for illustrating the example. Change types to your specific problem
  def run(input:RDD[String]) : RDD[String] = {
    // Your processing code here
    input.map(_.reverse)
  }

}
