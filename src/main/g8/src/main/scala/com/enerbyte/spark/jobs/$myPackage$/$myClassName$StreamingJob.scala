package com.enerbyte.spark.jobs.$myPackage$

import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.{Time, Duration, Seconds, StreamingContext}
import org.apache.spark.{SparkContext, SparkConf}

/**
 * Created by cebrian on 10/09/15.
 */
object $myClassName$StreamingJob {
  /**
   * This is the main function for the Spark job. Remove the ssc context in case you are working
   * on any batch processing functionality
   *
   * @param args
   */
  def main(args: Array[String]) = {
    val conf = new SparkConf()
       .setAppName("$name$")
    val sc = new SparkContext(conf)
    val ssc = new StreamingContext(sc, Seconds(1))

    // Create either the RDD or the DStream
    val files = ssc.textFileStream("s3://my-folder-with-files")

    run(files, Seconds(4), Seconds(2)) { (rdd:RDD[String], t:Time) =>
      rdd.foreach(println _)
    }

    ssc.checkpoint("s3://my-checkpoint-directory")
    ssc.start()
    ssc.awaitTermination()
  }

  // The RDD[String] is just for illustrating the example. Change types to your specific problem
  type StringHandler = (RDD[String], Time) => Unit

  // The DStream[String] is just for illustrating the example. Change types to your specific problem
  def run(input: DStream[String], windowDuration : Duration, slideDuration : Duration) (handler : StringHandler): Unit = {
    // Do your stuff here and send the final RDD to the handler for IO processing
    input.foreachRDD(handler)
  }

}
